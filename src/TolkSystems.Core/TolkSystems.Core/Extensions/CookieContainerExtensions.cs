﻿using System;
using System.Net;

public static class CookieContainerExtensions
{
    public static bool IsExpired(this CookieCollection cookies)
    {
        cookies.ThrowIfArgumentNull("cookies");

        if (cookies.Count <= 0)
            return true;

        foreach (Cookie item in cookies)
        {
            if (item.Expired || DateTime.Now >= item.Expires)
                return true;
        }

        return false;
    }

    public static bool IsExpired(this CookieContainer container, Uri uri)
    {
        container.ThrowIfArgumentNull("container");
        uri.ThrowIfArgumentNull("uri");

        var cookies = container.GetCookies(uri);
        if (cookies != null)
            return IsExpired(cookies);

        return true;
    }

    public static bool IsExpired(this CookieContainer container, string uri)
    {
        container.ThrowIfArgumentNull("container");
        uri.ThrowIfArgumentNullOrEmpry("uri");

        return IsExpired(container, new Uri(uri));
    }
}
