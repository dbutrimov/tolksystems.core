﻿using System;
using System.Collections.Generic;

public static class UriExtensions
{
    public static string GetQuery(this string uri)
    {
        int index = uri.LastIndexOf('?');
        if (index < 0)
            return string.Empty;

        string result = uri.Substring(index + 1, uri.Length - index - 1);
        return result;
    }

    public static Dictionary<string, string> GetQueryParams(this string query)
    {
        var result = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        string[] pairs = query.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
        foreach (string item in pairs)
        {
            string[] parts = item.Split('=');
            if (parts == null || parts.Length < 2)
                continue;

            result.Add(parts[0], parts[1]);
        }

        return result;
    }
}
