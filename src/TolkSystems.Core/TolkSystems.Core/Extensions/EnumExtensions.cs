﻿using System;

public static class EnumExtensions
{
    public static bool HasFlag(this Enum self, Enum flag)
    {
        var selfInt = Convert.ToInt32(self);
        var flagInt = Convert.ToInt32(flag);

        return (selfInt & flagInt) == flagInt;
    }
}
