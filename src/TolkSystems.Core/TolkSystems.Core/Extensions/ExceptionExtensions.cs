﻿using System;

public static class ExceptionExtensions
{
    public static void ThrowIfArgumentNull<T>(this T obj, string name)
        where T : class
    {
        if (obj == null)
            throw new ArgumentNullException(name);
    }

    public static void ThrowIfArgumentNullOrEmpry(this string str, string name)
    {
        if (string.IsNullOrEmpty(str))
            throw new ArgumentException(name + " is null or empty");
    }
}
